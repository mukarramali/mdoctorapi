class DoctorsController < ApplicationController

	def create
		doctor = Doctor.create!(doctor_params)
		json_response(doctor, :created)
	end

	def show
    	json_response Doctor.find params[:id]
  	end

	def index
		doctors = []
		Doctor.all.each do |doctor|
			item = doctor.attributes
			item[:label] = doctor[:name]
			doctors<<item
		end
		json_response doctors
	end

	private

		def doctor_params
			params.require(:doctor).permit(:name, :specialization, :phone).permit!
		end
end
