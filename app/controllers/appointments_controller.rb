class AppointmentsController < ApplicationController

	def create
		appointment = Appointment.create!(appointment_params)
		json_response(appointment, :created)
	end

	def show
    	json_response Appointment.find params[:id]
  	end

	def index
		appointments = []
		Appointment.all.each do |appointment|
			item = appointment.attributes
			item[:doctor_name] = appointment.doctor.name
			item[:patient_name] = appointment.patient.name
			appointments<<item
		end
		json_response appointments
	end

	private

		def appointment_params
			params.require(:appointment).permit(:patient_id, :doctor_id, :disease).permit!
		end
end
