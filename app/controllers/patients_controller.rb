class PatientsController < ApplicationController

	def create
		patient = Patient.create!(patient_params)
		json_response(patient, :created)
	end

	def show
    	json_response Patient.find params[:id]
  	end

	def index
		patients = []
		Patient.all.each do |patient|
			item = patient.attributes
			item[:label] = patient[:name]
			patients<<item
		end
		json_response patients
	end

	private

		def patient_params
			params.require(:patient).permit(:name, :phone).permit!
		end
end
