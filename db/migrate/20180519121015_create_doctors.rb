class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.string :name, null: false
      t.string :specialization, null: false

      t.timestamps null: false
    end
  end
end
